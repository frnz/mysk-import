pip install psycopg2

docker run -d --name postgres-mysk -e POSTGRES_PASSWORD=R5s@pT9!xYqZ -p 5432:5432 -v C:\data\postgres:/var/lib/postgresql/data postgres

docker start postgres-mysk


```sql
create table IF NOT EXISTS song
(
    id               integer generated always as identity,
    name             varchar(255),
    artist           varchar(255),
    originalfilename varchar(255)
        constraint song_originalfilename_key
            unique,
    fullpath         varchar(255),
    saved_at         varchar(255),
    created_at       varchar(255),
    rating           integer default 50 not null
);

create table IF NOT EXISTS songtag
(
    song_id integer,
    tag_id  integer,
    constraint uq_songid_tagid
        unique (song_id, tag_id)
);


create table IF NOT EXISTS stats
(
    song_id        integer           not null
        constraint stats_song_id_key
            unique,
    played_seconds integer default 0 not null,
    skipped_count  integer
);

create table IF NOT EXISTS tag
(
    id   integer generated always as identity,
    name varchar(255)
        constraint tag_name_key
            unique
);


```