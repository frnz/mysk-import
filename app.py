
import tkinter as tk
from tkinter import ttk
import psycopg2
from psycopg2 import Error
import os


db_params = {
    "host": "localhost",
    "database": "mysk",
    "user": "postgres",
    "password": "R5s@pT9!xYqZ"
}

config = {
    "input_path": "C:\music\download",
    "font": "Yu Gothic UI"
}


def update_db_config():
    global db_params

    db_params["host"] = textfield_pg_host.get()
    db_params["database"] = textfield_pg_database.get()
    db_params["user"] = textfield_pg_user.get()
    db_params["password"] = textfield_pg_pw.get()

    print(f"Db config = {db_params}")


def fetch_data():
    try:
        connection = psycopg2.connect(**db_params)

        cursor = connection.cursor()
        query = """
        SELECT s.*, t.name AS tag_name
        FROM song s
        LEFT JOIN songtag st ON s.id = st.song_id
        LEFT JOIN tag t ON st.tag_id = t.id;
        """
        cursor.execute(query)

        data = cursor.fetchall()

        result_text.delete(1.0, tk.END)  # Clear existing content
        for row in data:
            result_text.insert(tk.END, f"Row: {row}\n")

    except (Exception, Error) as e:
        result_text.insert(tk.END, f"Error: {e}\n")

    finally:
        if connection:
            cursor.close()
            connection.close()


def import_song(filename, songname, artist, tags):
    print("Importing:", filename)
    print("Songname:", songname)
    print("Artist:", artist)
    print("Tags:", tags)
    print("------")


def list_files():
    directory = path_entry.get()

    if os.path.isdir(directory):
        file_names = os.listdir(directory)

        for file_name in file_names:
            absolute_path = os.path.join(directory, file_name)
            add_row(absolute_path)
    else:
        print("Invalid directory path.")


def add_row(filename):
    row_frame = tk.Frame(scrollable_frame)
    row_frame.pack(fill="x")

    label = tk.Label(row_frame, text=os.path.basename(filename))
    label.pack(side="left")

    songname_entry = tk.Entry(row_frame)
    songname_entry.pack(side="left")

    artist_entry = tk.Entry(row_frame)
    artist_entry.pack(side="left")

    tags_entry = tk.Entry(row_frame)
    tags_entry.pack(side="left")

    import_button = tk.Button(row_frame, text="Import", command=lambda f=filename, a=artist_entry,
                              t=tags_entry, s=songname_entry: import_song(f, s.get(), a.get(), t.get()))
    import_button.pack(side="right")


# Create the main application window
root = tk.Tk()
root.title("MYSK Importer")
root.minsize(width=600, height=800)

# icon_path = "yl-dl-ui.ico"
# root.iconbitmap(icon_path)


# start frame
frame_pg_config = tk.Frame(root, width=100, height=50,
                           relief="flat", borderwidth=2)
size_label = 10
label_pg_config_header = tk.Label(frame_pg_config, text="Database Config",
                                  font=(config["font"], size_label, "bold"))
label_pg_config_header.grid(row=0, column=0,  sticky='w')

label_pg_config_host = tk.Label(frame_pg_config, text="host",
                                font=(config["font"], size_label, "bold"))
label_pg_config_host.grid(row=1, column=0,  sticky='w')
sv_pg_host = tk.StringVar()
textfield_pg_host = tk.Entry(
    frame_pg_config, textvariable=sv_pg_host, width=30)
textfield_pg_host.grid(row=1, column=1, sticky='w')

label_pg_config_database = tk.Label(frame_pg_config, text="database",
                                    font=(config["font"], size_label, "bold"))
label_pg_config_database.grid(row=2, column=0,  sticky='w')
sv_pg_database = tk.StringVar()
textfield_pg_database = tk.Entry(
    frame_pg_config, textvariable=sv_pg_database, width=30)
textfield_pg_database.grid(row=2, column=1, sticky='w')

label_pg_config_user = tk.Label(frame_pg_config, text="user",
                                font=(config["font"], size_label, "bold"))
label_pg_config_user.grid(row=1, column=2,  sticky='w')
sv_pg_user = tk.StringVar()
textfield_pg_user = tk.Entry(
    frame_pg_config, textvariable=sv_pg_user, width=30)
textfield_pg_user.grid(row=1, column=3, sticky='w')

label_pg_config_pw = tk.Label(frame_pg_config, text="host",
                              font=(config["font"], size_label, "bold"))
label_pg_config_pw.grid(row=2, column=2,  sticky='w')
sv_pg_pw = tk.StringVar()
textfield_pg_pw = tk.Entry(
    frame_pg_config, textvariable=sv_pg_pw, width=30, show='*')
textfield_pg_pw.grid(row=2, column=3, sticky='w')

textfield_pg_host.insert(0, db_params["host"])
textfield_pg_database.insert(0, db_params["database"])
textfield_pg_user.insert(0, db_params["user"])
textfield_pg_pw.insert(0, db_params["password"])
sv_pg_host.trace_add('write', lambda *args: update_db_config())
sv_pg_database.trace_add('write', lambda *args: update_db_config())
sv_pg_user.trace_add('write', lambda *args: update_db_config())
sv_pg_pw.trace_add('write', lambda *args: update_db_config())

frame_pg_config.pack(fill="x")

separator = ttk.Separator(root, orient='horizontal')
separator.pack(fill='x', padx=10, pady=10)

fetch_button = tk.Button(root, text="Fetch Data", command=fetch_data)
fetch_button.pack()

result_text = tk.Text(root, height=10, width=40)
result_text.pack()

separator2 = ttk.Separator(root, orient='horizontal')
separator2.pack(fill='x', padx=10, pady=10)

frame_song_files = tk.Frame(root)
frame_song_files.pack(padx=20, pady=20)

path_label = tk.Label(frame_song_files, text="Directory Path:")
path_label.grid(row=0, column=0, sticky="w")

path_entry = tk.Entry(frame_song_files)
path_entry.grid(row=0, column=1, sticky="w")
path_entry.insert(0, config["input_path"])

list_button = tk.Button(
    frame_song_files, text="List Files", command=list_files)
list_button.grid(row=1, columnspan=2)

separator3 = ttk.Separator(root, orient='horizontal')
separator3.pack(fill='x', padx=10, pady=10)

# Create a canvas and scrollbar for the scrollable container
canvas = tk.Canvas(root, height=300)
canvas.pack(fill="both", expand=True)
scrollbar = tk.Scrollbar(root, command=canvas.yview)
scrollbar.pack(side="right", fill="y")
canvas.configure(yscrollcommand=scrollbar.set)

# Create a frame to hold the scrollable content
scrollable_frame = tk.Frame(canvas)
canvas.create_window((0, 0), window=scrollable_frame, anchor="nw")

# Initialize the canvas height and row height
row_height = 30
root.mainloop()
