INSERT INTO song (name, artist, originalfilename, fullpath, saved_at, created_at, rating)
VALUES ('Song 1', 'Artist A', 'song1.mp3', '/path/to/song1.mp3', '2023-08-12', '2023-08-12', 70);

INSERT INTO song (name, artist, originalfilename, fullpath, saved_at, created_at, rating)
VALUES ('Song 2', 'Artist B', 'song2.mp3', '/path/to/song2.mp3', '2023-08-12', '2023-08-12', 60);

-- Add more INSERT statements for additional test data


INSERT INTO tag (name) VALUES ('Rock');
INSERT INTO tag (name) VALUES ('Pop');
INSERT INTO tag (name) VALUES ('Hip Hop');
INSERT INTO tag (name) VALUES ('Electronic');
-- Add more INSERT statements for additional test data


INSERT INTO songtag (song_id, tag_id) VALUES (1, 1); -- Song 1 tagged as Rock
INSERT INTO songtag (song_id, tag_id) VALUES (1, 2); -- Song 1 tagged as Pop
INSERT INTO songtag (song_id, tag_id) VALUES (2, 3); -- Song 2 tagged as Hip Hop
-- Add more INSERT statements for additional test data


INSERT INTO stats (song_id, played_seconds, skipped_count) VALUES (1, 150, 2); -- Song 1 stats
INSERT INTO stats (song_id, played_seconds, skipped_count) VALUES (2, 120, 1); -- Song 2 stats
-- Add more INSERT statements for additional test data
